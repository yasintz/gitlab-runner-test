const { execSync } = require('child_process');
const path = require('path');
const fs = require('fs');
const pm2 = require('pm2');

function promise(fn) {
  fn = fn.bind(pm2);
  return new Promise((resolve, reject) => {
    fn(function (err, res) {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
}

// process.env.HOME = '/Users/yasin/Documents/works/bthn';
const homeDir = process.env.HOME;

// Configuration
const repoUrl =
  'https://oauth2:glpat-_aRq23zQb6mGAxF3ebWe@gitlab.com/yasintz/gitlab-runner-test.git';
// 'git@gitlab.com:yasintz/gitlab-runner-test.git';
const repoFolderName = 'repo';
const localPath = path.join(homeDir, repoFolderName);
const branchName = 'main';
const fetchInterval = 60 * 1000; // Check every minute (in milliseconds)
const processName = 'nz-retail-test';

// Helper function to run a command and return the output
function runCommand(command, options = {}) {
  try {
    console.log(`Running: ${command}`);
    const response = execSync(command, { stdio: 'pipe', ...options })
      .toString()
      .trim();

    return response;
  } catch (err) {
    console.error(`Error running command: ${command}`, err);
    return null;
  }
}

// Initialize repository
function initializeRepo() {
  if (!fs.existsSync(localPath)) {
    process.chdir(homeDir);
    runCommand(`git clone ${repoUrl} ${repoFolderName}`);
  } else {
    console.log(`Repository already cloned at ${localPath}`);
  }

  process.chdir(localPath);
  runCommand('git fetch');
  runCommand(`git checkout ${branchName}`);
  runCommand(`git branch --set-upstream-to=origin/${branchName} ${branchName}`);
  console.log(`Checked out and tracking branch ${branchName}`);
}

// Function to check if the origin has changed
function hasOriginChanged() {
  process.chdir(localPath);
  const localHash = runCommand(`git rev-parse ${branchName}`);
  runCommand('git fetch');
  const remoteHash = runCommand(`git rev-parse origin/${branchName}`);
  return localHash !== remoteHash;
}

// Function to run if origin changed
async function onOriginChange() {
  process.chdir(localPath);
  runCommand(`git merge origin/${branchName}`);
  const list = await promise(pm2.list);
  const isNzRetailRunning = list.find((i) => i.name === processName);

  if (isNzRetailRunning) {
    await promise((f) => pm2.stop(processName, f));
    await promise((f) => pm2.delete(processName, f));
  }

  await promise((f) =>
    pm2.start(
      {
        cwd: localPath,
        script: 'run.sh',
        name: processName,
      },
      f
    )
  );
}

async function main() {
  await promise((f) => pm2.connect(f));

  initializeRepo();
  await onOriginChange();

  setInterval(() => {
    if (hasOriginChanged()) {
      onOriginChange();
    } else {
      console.log(`No changes in origin/${branchName}`);
    }
  }, fetchInterval);
}

main();
